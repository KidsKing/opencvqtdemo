#include "mygraphicsview.h"
#include <QDebug>


MyGraphicsView::MyGraphicsView(QWidget *parent) : QGraphicsView(parent)
{
    isMouseTranslate=false;
}

void MyGraphicsView::zoomIn(qreal delta)
{
    zoom(delta);
}

void MyGraphicsView::zoomOut(qreal delta)
{
    zoom(1/delta);
}

void MyGraphicsView::zoom(float scaleFactor)
{

    setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
    qreal factor = transform().scale(scaleFactor, scaleFactor).mapRect(QRectF(0, 0, 1, 1)).width();
    if (factor < 0.01 || factor > 2000)
        return;
    scale(scaleFactor, scaleFactor);
}

void MyGraphicsView::translate(QPointF delta)
{
    //根据鼠标点作为锚点来定位scene
    setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
    QPoint newCenter(this->viewport()->rect().width() / 2 - delta.x(),  this->viewport()->rect().height() / 2 - delta.y());
    centerOn(mapToScene(newCenter));//将scene的坐标系中的指定坐标点newCenter与view中心点对齐

    //将view的中心点作为锚点来定位scene
    setTransformationAnchor(QGraphicsView::AnchorViewCenter);
}

void MyGraphicsView::mouseMoveEvent(QMouseEvent *event)
{
    if (isMouseTranslate){
        QPointF mouseDelta = event->pos() - lastMousePos;//移动量不要mapToScene，否则随着图像放大移动量会变小
        translate(mouseDelta);
        //this->translate(mouseDelta.x(), mouseDelta.y());
    }

    lastMousePos = event->pos();
    QGraphicsView::mouseMoveEvent(event);
}

void MyGraphicsView::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::MouseButton::LeftButton)
    {
        isMouseTranslate = true;
        lastMousePos = event->pos();
    }

    QGraphicsView::mousePressEvent(event);
}

void MyGraphicsView::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::MouseButton::LeftButton)
        isMouseTranslate = false;
    QGraphicsView::mouseReleaseEvent(event);
}

void MyGraphicsView::wheelEvent(QWheelEvent* event)
{
    if (event->modifiers() == Qt::ControlModifier)//判断是否按下ctrl
    {
        if (event->delta() > 0)//向上滚动为证 向下滚动为负
        {
            this->zoomIn(0.9);
        }
        else
        {
            this->zoomOut(0.9);
        }
    }
}
