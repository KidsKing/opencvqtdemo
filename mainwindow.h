#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsPixmapItem>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <QDir>
#include <QFileDialog>

#include "mygraphicsview.h"
#include "imagemanager.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    QGraphicsScene * m_scene;
    QGraphicsPixmapItem * m_imageItem;
    QGraphicsScene* m_scene1;
    QGraphicsPixmapItem* m_imageItem1;

    imageManager *imgManager;

    int imageHeight;
    int imageWidth;
    bool filePathGet(const QString& path);
    QStringList filePath;
    
public slots:
    void sliceChange(int slice);
    void imageShowIni(int num);
private slots:
    void on_openAction_triggered();

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
