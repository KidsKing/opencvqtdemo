#ifndef IMAGEMANAGER_H
#define IMAGEMANAGER_H

#include <QObject>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <QImage>
#include <QFile>
#include <QDir>
#include <QMap>

class imageManager : public QObject
{
    Q_OBJECT
public:
    explicit imageManager();

    cv::Mat imageRead(QString fileStr, int rawDataWidth, int rawDataHeight);

    QImage cvMat2QImage(const cv::Mat& mat);
    QMap<int, QList<cv::Mat>> imgMap;

    void dataImport(QStringList path, int imageWidth, int imageHeight);

signals:
    void signalImageShow(int num);

};

#endif // IMAGEMANAGER_H
