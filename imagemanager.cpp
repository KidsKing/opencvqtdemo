#include "imagemanager.h"
#include <QDebug>
#include <QProgressDialog>
#include <QCoreApplication>

imageManager::imageManager()
{

}

cv::Mat imageManager::imageRead(QString fileStr, int rawDataWidth, int rawDataHeight)
{
    FILE *filePointer;
    QByteArray byte = fileStr.toLatin1();
    char *file_name_path = byte.data();
    fopen_s(&filePointer, file_name_path, "rb+");
    if (filePointer == NULL)
    {
        cv::Mat img;
        return img;
    }
    float *ptr_data32 = new float;
    QVector<float> pBit;

    //读取Mat数据  
    for (int i = 0; i < rawDataHeight; i++)
    {
        for (int j=0; j<rawDataWidth; j++)
        {
            fread(ptr_data32, 1, sizeof(int), filePointer);
            pBit.push_back(*ptr_data32);
        }
    }
    QVector<float>::iterator m = std::max_element(pBit.begin(), pBit.end());
    QVector<float>::iterator n = std::min_element(pBit.begin(), pBit.end());

    fclose(filePointer);
    cv::Mat img(rawDataHeight,rawDataWidth, CV_8UC1);
    for (int i=0;i<rawDataHeight*rawDataWidth;i++)
    {
        img.data[i]= (pBit[i] / (*m- *n)) * 255;
    }
    //cv::Mat dst;
    //normalize(img, dst, 0, 1, cv::NORM_MINMAX);
    return img;
}

QImage imageManager::cvMat2QImage(const cv::Mat& mat)
{
    // 8-bits unsigned, NO. OF CHANNELS = 1
    if (mat.type() == CV_8UC1)
    {
        QImage image(mat.cols, mat.rows, QImage::Format_Indexed8);
        // Set the color table (used to translate colour indexes to qRgb values)
        image.setColorCount(256);
        for (int i = 0; i < 256; i++)
        {
            image.setColor(i, qRgb(i, i, i));
        }
        // Copy input Mat
        uchar* pSrc = mat.data;
        for (int row = 0; row < mat.rows; row++)
        {
            uchar* pDest = image.scanLine(row);
            memcpy(pDest, pSrc, mat.cols);
            pSrc += mat.step;
        }
        return image;
    }
    // 8-bits unsigned, NO. OF CHANNELS = 3
    else if (mat.type() == CV_8UC3)
    {
        // Copy input Mat
        const uchar* pSrc = (const uchar*)mat.data;
        // Create QImage with same dimensions as input Mat
        QImage image(pSrc, mat.cols, mat.rows, mat.step, QImage::Format_RGB888);
        return image.rgbSwapped();
    }
    else if (mat.type() == CV_8UC4)
    {
        // Copy input Mat
        const uchar* pSrc = (const uchar*)mat.data;
        // Create QImage with same dimensions as input Mat
        QImage image(pSrc, mat.cols, mat.rows, mat.step, QImage::Format_ARGB32);
        return image.copy();
    }
    else
    {
        return QImage();
    }
}

void imageManager::dataImport(QStringList path, int imageWidth, int imageHeight)
{
    for (int i = 0; i < path.size(); i++)
    {
        QDir dir(path[i]);
        QStringList nameFilters;
        nameFilters << "*.raw";
        QStringList files = dir.entryList(nameFilters, QDir::Files | QDir::Readable, QDir::Name);
        QProgressDialog* progressDlg = new QProgressDialog(QString("床位 %1 数据导入中").arg(i + 1), "取消", 0, files.count());
        progressDlg->setWindowTitle("导入");
        progressDlg->setWindowModality(Qt::WindowModal);    //非模态对话框
        if (!files.isEmpty())
        {
            for (int j = 0; j < files.count(); j++)
            {
                imgMap[i].append(imageRead(path[i] + "/" + files[j], imageWidth, imageHeight));
                progressDlg->setValue(j + 1);
                QCoreApplication::processEvents();
                if (progressDlg->wasCanceled())
                    break;
            }
        }
        emit signalImageShow(i);
    }
}
