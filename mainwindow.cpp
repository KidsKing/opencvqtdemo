#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QGraphicsScene>
#include <QDebug>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    imgManager=new imageManager;
    imageHeight=2940;
    imageWidth=2940;

    m_scene=new QGraphicsScene;
    m_imageItem = new QGraphicsPixmapItem();
    m_scene1 = new QGraphicsScene;
    m_imageItem1 = new QGraphicsPixmapItem();
    //场景中添加画布
    m_scene->addItem(m_imageItem);
    ui->graphicsView->setScene(m_scene);
    m_scene1->addItem(m_imageItem1);
    ui->graphicsView1->setScene(m_scene1);
    //ui->graphicsView->installEventFilter(this);
    
    connect(ui->horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT(sliceChange(int)));
    connect(ui->horizontalSlider1, SIGNAL(valueChanged(int)), this, SLOT(sliceChange(int)));
    connect(imgManager, SIGNAL(signalImageShow(int)), this, SLOT(imageShowIni(int)));
    
}

MainWindow::~MainWindow()
{
    delete ui;
}

bool MainWindow::filePathGet(const QString& path)
{
    QDir dir(path);
    if (!dir.exists()) {
        return false;
    }
    dir.setFilter(QDir::Dirs);
    dir.setSorting(QDir::DirsFirst);
    QFileInfoList list = dir.entryInfoList();
    int i = 0;
    do {

        QFileInfo fileInfo = list.at(i);
        if (fileInfo.fileName() == "." | fileInfo.fileName() == "..") {
            i++;
            continue;
        }
        bool bisDir = fileInfo.isDir();
        if (bisDir) {
            filePathGet(fileInfo.filePath());
            filePath << fileInfo.filePath();
        }
        i++;
    } while (i < list.size());
    return true;
}

void MainWindow::imageShowIni(int num)
{
    switch (num)
    {
    case 0:
    {
        ui->horizontalSlider->setMaximum(imgManager->imgMap[0].size() - 1);
        m_imageItem->setPixmap(QPixmap::fromImage(imgManager->cvMat2QImage(imgManager->imgMap[0][0])));
        ui->graphicsView->show();
        break;
    }
    case 1:
    {
        ui->horizontalSlider1->setMaximum(imgManager->imgMap[1].size() - 1);
        m_imageItem1->setPixmap(QPixmap::fromImage(imgManager->cvMat2QImage(imgManager->imgMap[1][0])));
        ui->graphicsView1->show();
        break;
    }
    default:
        break;
    }   
}

void MainWindow::sliceChange(int slice)
{
    QSlider* btn = qobject_cast<QSlider*>(sender());
    if (btn == ui->horizontalSlider)
    {
        m_imageItem->setPixmap(QPixmap::fromImage(imgManager->cvMat2QImage(imgManager->imgMap[0][slice])));
        ui->graphicsView->viewport()->update();
    }
    else if (btn == ui->horizontalSlider1)
    {
        m_imageItem1->setPixmap(QPixmap::fromImage(imgManager->cvMat2QImage(imgManager->imgMap[1][slice])));
        ui->graphicsView1->viewport()->update();
    }   
}


void MainWindow::on_openAction_triggered()
{
    QString fileDerectory=QFileDialog::getExistingDirectory(this,"打开路径","/");
    filePathGet(fileDerectory);
    imgManager->dataImport(filePath, imageWidth, imageHeight);

 /*   QDir dir(fileDerectory);
    QStringList nameFilters;
    nameFilters << "*.raw";
    QStringList files = dir.entryList(nameFilters, QDir::Files|QDir::Readable, QDir::Name);
    if (!files.isEmpty())
    {
        QProgressDialog *progressDlg=new QProgressDialog("载入中","取消",0,files.count(),this);
        progressDlg->setWindowTitle("载入");
        progressDlg->setWindowModality(Qt::WindowModal);
        progressDlg->show();
        for (int i = 0; i < files.count(); i++)
        {
            imgManager->imgList.append(imgManager->imageRead(fileDerectory+"/"+files[i],imageWidth,imageHeight));
            progressDlg->setValue(i+1);
            QCoreApplication::processEvents();
            if (progressDlg->wasCanceled())
                break;
        }
        progressDlg->setValue(files.count());
        ui->horizontalSlider->setMaximum(imgManager->imgList.size()-1);
        m_imageItem->setPixmap(QPixmap::fromImage(imgManager->cvMat2QImage(imgManager->imgList[0])));
        ui->graphicsView->show();
    }*/
}
